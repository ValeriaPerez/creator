import React, {PropTypes, Component} from 'react';

class YouTube extends Component {
  render() {
    const {resource} = this.props;
    const videoID = resource.link.split('?v=')[1]
    return (
      <iframe src={`https://www.youtube.com/embed/${videoID}`} allowFullScreen></iframe>
    );
  }
}

YouTube.porpTypes = {
  resource: PropTypes.object
};

class Web extends Component {
  render() {
    const {resource} = this.props;
    return (
      <iframe src={resource.link}></iframe>
    );
  }
}

Web.porpTypes = {
  resource: PropTypes.object
};

class Text extends Component {

  createMarkup(markup) {
    return {__html: markup};
  }

  render() {
    const {resource} = this.props;
    return (
      <div className="text-frame" dangerouslySetInnerHTML={this.createMarkup(resource)}></div>
    );
  }
}

Text.porpTypes = {
  resource: PropTypes.object
};

export {
  YouTube, Web, Text
}
