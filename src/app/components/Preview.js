import React, {PropTypes, Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as TodoActions from '../actions/index';
import * as PrevElems from './previewElement';

class Preview extends Component {

  state = {
    index: 0
  }

  next = () => {
    const {index} = this.state;
    const resources = this.props.resources.resources.filter(resource => Object.keys(resource).length > 0 && typeof resource !== 'undefined');
    if(index + 1 >= this.props.resources.length - 2 ){
      return;
    } else {
      this.setState({
        index: index + 1
      });
    }
  }

  render() {
    const {actions, resources} = this.props;
    const renderedRes = resources.resources.filter(resource => Object.keys(resource).length > 0 && typeof resource !== 'undefined');
    let component = null;
    switch(renderedRes[this.state.index].type) {
      case 'youtube':
        component = <PrevElems.YouTube resource={renderedRes[this.state.index].resource}/>
        break;
      case 'wikipedia':
      case 'google':
        component = <PrevElems.Web resource={renderedRes[this.state.index].resource}/>
        break;
      case 'text':
        component = <PrevElems.Text resource={renderedRes[this.state.index].resource}/>
        break;

      default:
        console.log(renderedRes[this.state.index].type);
    }

    return (
      <div className="preview">
        <div className="preview-menu">
          <div>
            <button onClick={actions.hidePreview}>
              <i className="fa fa-times" aria-hidden="true"></i>
            </button>
          </div>
          <div className="preview-title">
            <h2>{renderedRes[this.state.index].resource.title}</h2>
          </div>
        </div>
        <div className="preview-content">
          {this.state.index !== 0 ? (
            <button className="preview-btn-left" onClick={this.prev}>
              <i className="fa fa-chevron-left" aria-hidden="true"></i>
            </button>
          ) : null}
          {this.state.index < renderedRes.length ? (
            <button className="preview-btn-right" onClick={this.next}>
              <i className="fa fa-chevron-right" aria-hidden="true"></i>
            </button>
          ) : null}
          {component}
        </div>
      </div>
    );
  }
}

Preview.propTypes = {
  panel: React.PropTypes.object,
  actions: React.PropTypes.object
};

function mapStateToProps(state) {
  return {
    panel: state.panel,
    resources: state.resources
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(TodoActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Preview);
