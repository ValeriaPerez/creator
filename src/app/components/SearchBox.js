import React, {PropTypes, Component} from 'react';
import classnames from 'classnames';
import {Field, reduxForm} from 'redux-form';

import RaisedButton from 'material-ui/RaisedButton';
import {TextField} from 'redux-form-material-ui';


class SearchBox extends Component {

  updateTypeSearch(type) {
    this.props.updateTypeSearch(type);
  }

  render() {
    const {handleSubmit} = this.props;
    return (
      <form onSubmit={handleSubmit}>
        <Field
          name="search"
          component={TextField}
          hintText="Street"
          />
        <RaisedButton
          type="submit"
          label="med"
          onClick={this.updateTypeSearch.bind(this, 'med')}
          />
        <RaisedButton
          type="submit"
          icon={<i className="fa fa-youtube-play" aria-hidden="true"></i>}
          onClick={this.updateTypeSearch.bind(this, 'youtube')}
          />
        <RaisedButton
          type="submit"
          icon={<i className="fa fa-google" aria-hidden="true"></i>}
          onClick={this.updateTypeSearch.bind(this, 'google')}
          />
        <RaisedButton
          type="submit"
          icon={<i className="fa fa-wikipedia-w" aria-hidden="true"></i>}
          onClick={this.updateTypeSearch.bind(this, 'wikipedia')}
          />
        <RaisedButton
          type="submit"
          icon={<i className="fa fa-desktop" aria-hidden="true"></i>}
          onClick={this.updateTypeSearch.bind(this, 'desktop')}
          />
      </form>
    );
  }
}

SearchBox.propTypes = {
  searching: React.PropTypes.string,
  updateTypeSearch: React.PropTypes.func
};

SearchBox = reduxForm({
  form: 'search'
})(SearchBox);

export default SearchBox;
