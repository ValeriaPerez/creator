import React, {PropTypes, Component} from 'react';
import {GoogleResult, YoutubeResult, MEDResult, WikipediaResult} from './itemResults';
import RaisedButton from 'material-ui/RaisedButton';
import classnames from 'classnames';

const style = {};

class GridElementOccupied extends Component {

  constructor(props) {
    super(props);
    this.state = {
      colored: false
    };
  }

  allowDrop = (event) => {
    event.preventDefault();
    this.setState({colored: true});
  }

  disableDrop = (event) => {
    event.preventDefault();
    this.setState({colored: false});
  }

  handleDrop = (event) => {
    event.preventDefault();
    this.setState({colored: false});
    this.props.handleDrop(this.props.coords, event.dataTransfer.getData("text"));
  }

  showMED(resource) {
    let image = null;
    if(resource.pagemap.cse_image){
      if(resource.pagemap.cse_image.length > 0) {
        image = resource.pagemap.cse_image[0].src;
      }
    }
    return (
      <div className="grid-content">
        <div className="grid-background">
          <img src={image}/>
        </div>
        <div className="grid-foot">
          <div>
            <h3>{resource.title}</h3>
          </div>
          <div className="grid-type">
            <span>MED</span>
          </div>
        </div>
      </div>
    );
  }

  showVideo(resource) {
    let image = null;
    if(resource.pagemap.cse_image){
      if(resource.pagemap.cse_image.length > 0) {
        image = resource.pagemap.cse_image[0].src;
      }
    }
    return (
      <div className="grid-content">
        <div className="grid-background">
          <img src={image}/>
        </div>
        <div className="grid-play"><i className="fa fa-play" aria-hidden="true"></i></div>
        <div className="grid-foot">
          <div>
            <h3>{resource.title}</h3>
          </div>
          <div className="grid-type">
            <span><i className="fa fa-youtube-play" aria-hidden="true"></i></span>
          </div>
        </div>
      </div>
    );
  }

  showGoogle(resource) {
    let image = null;
    if(resource.pagemap.cse_image){
      if(resource.pagemap.cse_image.length > 0) {
        image = resource.pagemap.cse_image[0].src;
      }
    }
    return (
      <div className="grid-content">
        <div className="grid-background">
          <img src={image}/>
        </div>
        <div className="grid-foot">
          <div>
            <h3>{resource.title}</h3>
          </div>
          <div className="grid-type">
            <span className="grid-type-google">
              <i className="fa fa-google" aria-hidden="true"></i>
            </span>
          </div>
        </div>
      </div>
    );
  }

  showWiki(resource) {
    return (
      <div className="grid-content">
        <div className="grid-background">
          <img src="/assets/wikipedia.jpg"/>
        </div>
        <div className="grid-foot">
          <div>
            <h3>{resource.title}</h3>
          </div>
          <div className="grid-type">
            <span className="grid-type-google">
              <i className="fa fa-wikipedia-w" aria-hidden="true"></i>
            </span>
          </div>
        </div>
      </div>
    );
  }

  showText(resource) {
    const createMarkup = function(markup) {
      return {__html: markup};
    }
    return (
      <div className="grid-content">
        <div className="grid-text">
          <div dangerouslySetInnerHTML={createMarkup(resource)}></div>
        </div>
        <div className="grid-foot">
          <div>
            <h3>Editar texto</h3>
          </div>
          <div className="grid-type">
            <span className="">T</span>
          </div>
        </div>
      </div>
    );
  }

  render() {
    let styles = null;

    const resource = this.props.resource;

    let content = (
      <div className="grid-content">
        <div className="drop-here">
          <i className="fa fa-chevron-circle-down" aria-hidden="true"></i>
          <div>Arrastra aquí tu recurso</div>
        </div>
        <div className="options">
          <RaisedButton
            icon={<i className="fa fa-font" aria-hidden="true"></i>}
            />
        </div>
      </div>
    );

    if(this.state.colored) {
      styles = classnames('grid-elem', 'grid-elem-hover');
    } else {
      styles = classnames('grid-elem');
    }

    switch(resource.type) {
      case 'med': {
        content = this.showMED(resource.resource);
        break;
      }
      case 'youtube': {
        content = this.showVideo(resource.resource);
        break;
      }
      case 'google': {
        content = this.showGoogle(resource.resource);
        break;
      }
      case 'wikipedia': {
        content = this.showWiki(resource.resource);
        break;
      }

      case 'text': {
        content = this.showText(resource.resource);
        break;
      }
    }

    return (
      <div className={styles} onDrop={this.handleDrop} onDragOver={this.allowDrop} onDragLeave={this.disableDrop}>
        {content}
      </div>
    );
  }
}

GridElementOccupied.propTypes = {
  coords: React.PropTypes.object,
  handleDrop: React.PropTypes.func,
  resource: React.PropTypes.object
};

export default GridElementOccupied;
