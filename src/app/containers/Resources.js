import React, {Component, PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as TodoActions from '../actions/index';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

import SearchBox from '../components/SearchBox';
import Results from '../components/Results';
import Grid from '../components/Grid';
import Preview from '../components/Preview';

const style = {
  display: 'flex',
  justifyContent: 'flex-end'
}

class Resources extends Component {

  handleSubmit = (values) => {
    const {actions, resources} = this.props;
    if(resources.type !== "desktop") {
      actions.fetchResources(resources.type, values.search);
    }
  }

  loadMore = () => {
    const {actions} = this.props;
    actions.loadMore();
  }

  updateTypeSearch = (type) => {
    this.props.actions.updateTypeSearch(type);
  }

  renderMoreResults() {
    const {resources} = this.props;
    if(resources.next) {
      return (
        <div>
          <button onClick={this.loadMore}>Cargar más</button>
        </div>
      );
    }
  }

  renderResults() {
    const {resources} = this.props;
    if(resources.loading) {
      return <div className="results"><img className="loading" src="assets/loading.gif" /></div>;
    } else {
      return (
        <Results results={resources.stream} type={resources.type}>
          {this.renderMoreResults()}
        </Results>);
    }
  }

  handleShowPreview = () => {
    const {actions} = this.props;
    actions.showPreview();
  }

  renderModals = () => {
    const {panel} = this.props;
    return (
      <div>
        {panel.showPreview ? <Preview/> : null}
      </div>
    );
  }

  render() {
    const {resources, actions} = this.props;
    return (
      <MuiThemeProvider>
        <div className="main">
          {this.renderModals()}
          <header>
            <div className="row">
              <div className="col-md-6">
                <SearchBox
                  onSubmit={this.handleSubmit}
                  updateTypeSearch={this.updateTypeSearch}
                  />
              </div>
              <div className="col-md-6">
                <div style={style}>
                  <RaisedButton
                    label="Regresar al listado"
                    icon={<i className="fa fa-long-arrow-left" aria-hidden="true"></i>}
                    />
                  <RaisedButton
                    label="Ver planeación"
                    icon={<i className="fa fa-play" aria-hidden="true"></i>}
                    onClick={this.handleShowPreview}
                    />
                  <RaisedButton
                    label="Guardar"
                    onClick={actions.saveResource}
                    icon={<i className="fa fa-floppy-o" aria-hidden="true"></i>}
                    />
                </div>
              </div>
            </div>
          </header>
          <div className="panel">
            {this.renderResults()}
            <Grid />
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

Resources.propTypes = {
  resources: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    resources: state.resources,
    panel: state.panel
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(TodoActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Resources);
