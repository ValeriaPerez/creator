import {
  SHOW_PREVIEW,
  HIDE_PREVIEW
} from '../constants/ActionTypes';

const initialState = {
  showPreview: true
};

export default function panel(state = initialState, action) {
  switch (action.type) {

    case SHOW_PREVIEW:
      return {
        ...state,
        showPreview: true
      };

    case HIDE_PREVIEW:
      return {
        ...state,
        showPreview: false
      };

    default:
      return state;
  }
}
