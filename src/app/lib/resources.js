export function getConfig(type, query) {
  let cx = null;
  switch(type) {
    case 'google': {
      cx = "011002903987599304646:tfpcehnkfdi";
      break;
    }
    case 'youtube': {
      cx = '011002903987599304646:avijxnul0mk';
      break;
    }
    case 'med': {
      cx = '011002903987599304646:xleqjrspq4a';
      break;
    }
    case 'wikipedia': {
      cx = '011002903987599304646:v3zb4ml1abq';
      break;
    }
    case 'desktop': {
      break;
    }
  }

  return {
    url: "https://www.googleapis.com/customsearch/v1",
    params: {
      key: "AIzaSyB0nljZFyGSv3QsGLQLMgIBt6V3ug85Slw",
      cx: cx,
      q: encodeURIComponent(query),
      start: 1,
      safe: "high"
    }
  };
}
